<?php

namespace App\Tests\Api;

use ApiPlatform\Symfony\Bundle\Test\ApiTestCase;
use App\Entity\MediaFile;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class MediaFileTest extends ApiTestCase
{
    public function testPostMediaFile(): void
    {
        $file = new UploadedFile('fixtures/files/image.png', 'image.png');
        static::createClient()->request('POST', '/media_files', [
            'headers' => ['Content-Type' => 'multipart/form-data'],
            'extra' => [
                'files' => [
                    'file' => $file,
                ],
            ]
        ]);
        $this->assertResponseIsSuccessful();
        $this->assertMatchesResourceItemJsonSchema(MediaFile::class);
    }

    /**
     * @depends testPostMediaFile
     */
    public function testGetCollectionMediaFile(): void
    {
        static::createClient()->request('GET', '/media_files');
        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        $this->assertMatchesResourceCollectionJsonSchema(MediaFile::class);
    }

    /**
     * @depends testGetCollectionMediaFile
     */
    public function testGetMediaFile(): void
    {
        $iri = $this->findIriBy(MediaFile::class, ['id' => 1]);
        static::createClient()->request('GET', $iri);
        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        $this->assertMatchesResourceItemJsonSchema(MediaFile::class);
    }

    /**
     * @depends testGetMediaFile
     */
    public function testDeleteMediaFile(): void
    {
        $iri = $this->findIriBy(MediaFile::class, ['id' => 1]);
        static::createClient()->request('DELETE', $iri);
        $this->assertResponseStatusCodeSame(204);
    }

}
