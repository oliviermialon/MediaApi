# MediaApi

### Stack

- PHP 8.1
- Symfony 6.1
- API Platform 3
- Docker

### Installation requirements
- Docker
- Docker Compose

### Setup guide

1) If Docker Compose version >= 2, run :
````bash
docker compose build --pull --no-cache
docker compose up -d
````

If Docker Compose version < 2, run :
````bash
docker-compose build --pull --no-cache
docker-compose up -d
````

2) Go to https://localhost
