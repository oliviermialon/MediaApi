<?php

namespace App\Controller;

use App\Entity\MediaFile;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

#[AsController]
final class AddMediaFileAction extends AbstractController
{
    public function __invoke(Request $request): MediaFile
    {
        $uploadedFile = $request->files->get('file');
        if (!$uploadedFile) {
            throw new BadRequestHttpException('An input media file is required.');
        }

        /*
        if($uploadedFile->getMimeType() != "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"){
            throw new BadRequestHttpException('X');
        }
        */

        $mediaObject = new MediaFile();
        $mediaObject->file = $uploadedFile;

        return $mediaObject;
    }
}
